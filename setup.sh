#!/bin/bash

# Small script for installing dot-files.

current_path=$(realpath `dirname $BASH_SOURCE`)
cd $current_path
echo "Path of the repository: $current_path"

ask() {
    echo -n "$1 [Y/n] "
    read input
    if [[ $input =~ ^(y|Y)?$ ]]; then
        return 0
    fi
    return 1
}

ln_dir_content() {
    source=$2
    target=$3
    ask "Use $3 as $1 directory?" || return 1
    mkdir -p $3 
    files=`ls -A $source`
    for file in $files; do
        ask "Install $file ?" || continue
        ln -s $source/$file $target 2> /dev/null \
        && echo "Successfully created symbolic link for $file" \
        || echo "Failed to create symbolic link for $file"
    done


}

unlink_dir_content() {
    source=$1
    target=$2
    echo "Removing content of $1"
    files=`ls -A $source`
    for file in $files; do
        if [ "$(readlink $2/$file)" == "$1/$file" ]; then
            echo "removing link $1/$file"
            unlink "$2/$file"
        fi
    done
}


config=$XDG_CONFIG_HOME
if [ ! -d "$dir" ]; then    
    config=$HOME/.config
    echo "Environment variable \$XDG_CONFIG_HOME not found or invalid, falling back to $config"
fi

bin=$HOME/.local/bin

if [ $1 == "unlink" ]; then
    unlink_dir_content "$PWD/config" "$config"
    unlink_dir_content "$PWD/bin" "$bin"
    unlink_dir_content "$PWD/home" "$HOME"
    exit 0;
fi

ln_dir_content "config" "$PWD/config" "$config"
ln_dir_content "bin" "$PWD/bin" "$bin"
ln_dir_content "home" "$PWD/home" "$HOME"
